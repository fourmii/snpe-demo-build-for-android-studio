开发前的配置要求
Android Studio
android-ndk-r23c

安装步骤

IDE 自己安装就好啦。

git clone https://gitee.com/fourmii/snpe-demo-build-for-android-studio.git

主要文件目录说明

SnpeDemo\app\src\main\cpp\ 

├── CMakeLists.txt

├── src

├── include

├── inc

├── armeabi-v7a

└── arm64-v8a

CMakeLists.txt: 包含编译代码配置相关。

src: 存了一些源码啦。
include: 存的SNPE库的头文件。

inc: 存的opencv的头文件

arm64-v8a: 存的是依赖的一些arm64位的动态库，包含SNPE和Opencv的(我已经编译好了的)直接用即可，注意是要在Android平台哦.

armeabi-v7a:同arm64-v8a,不过是32位的。

邮箱: fourmi2017@gmail.com

知乎:fourmi
